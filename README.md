# Dorfromantik-Gamesheet

## What is this?

This application is a little helper for all who love the award winning game ["Dorfromantik"](https://pegasus.de/15245/dorfromantik-das-brettspiel-spiel-des-jahres-2023). 

It can assist you in calculating the points scored during a round of "Dorfromantik".
The layout is reflecting the scorecards contained in the box.

## Who can use it?

The application is hosted for free for anybody as long as the hosting is free. There is no guarantee, that the project will kept online. 

See LICENSE file for further information about the license of the source code.

## Why? 

The project was created as a hobby project in order to learn somethings about the Framework Angular.