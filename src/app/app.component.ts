import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { GameSheetComponent } from './game-sheet/game-sheet.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, GameSheetComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'Dorfromantik-Gamesheet';
}
