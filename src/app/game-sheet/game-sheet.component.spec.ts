import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameSheetComponent } from './game-sheet.component';
import { Territory } from '../game-sheet';

describe('GameSheetComponent', () => {
  let component: GameSheetComponent;
  let fixture: ComponentFixture<GameSheetComponent>;

    beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [GameSheetComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(GameSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  [["forest","tree"], ["field","square"], ["village","house"], ["railroad","train-front"],["river","droplet"]]
  .forEach((classAndIcon) => {
    it('should have icons for territories', () => {
      const element = fixture.nativeElement as HTMLElement;
      expect(element.querySelector(`i.territory.${classAndIcon[0]}`)).toHaveClass(`bi-${classAndIcon[1]}-fill`);
    })
  });

  it('roundCounter should have two way binding', () => {
    
    const element = fixture.nativeElement as HTMLElement;
    const component = fixture.componentInstance as GameSheetComponent;

    component.gameSheet.round = randomInt();
    const counterElement = element.querySelector("#roundCounter") as HTMLInputElement;
    expect(counterElement).toBeDefined();
    
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      
      expect(counterElement.valueAsNumber).toBe(component.gameSheet.round);

      counterElement.valueAsNumber = randomInt();
      counterElement.dispatchEvent(new Event('input'));
      expect(component.gameSheet.round = counterElement.valueAsNumber);
    });
  });

  Object.values(Territory)
  .forEach((territory) => {
    it(`change on quest counter for ${territory} should change questsum and overall sum `, () => {
      checkPointField(
        fixture, 
        `#${territory.toLowerCase()}QuestPoints`, 
        () => fixture.componentInstance.gameSheet.questSum(),
        points => fixture.componentInstance.gameSheet.quests[territory] = points
      );
    });
  });

  Object.values(Territory)
  .forEach((territory) => {
    it(`changes in connected fields should change intermediate sum and overall sum`, () => {
      const gameSheet = fixture.componentInstance.gameSheet;
      checkPointField(
        fixture,
        `#${territory.toLowerCase()}ConnectedPoints`,
        () => gameSheet.connectedSum(),
        points => gameSheet.connected[territory] = points
      );
    });
  });

  it(`should map date to/from gamesheet`, () => {
    const component = fixture.componentInstance;
    const element   = fixture.nativeElement as HTMLElement;
    const dateElement = element.querySelector("#gameDate") as HTMLInputElement;

    /*
     * change model and check binding to view
     */
    let testDate = new Date("2024-02-21");
    component.gameSheet.date = testDate;

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(dateElement.valueAsDate).toEqual(testDate);
    });

    /*
     * change view and check binding to model
     */
    testDate = new Date("2024-02-22");
    dateElement.value = "2024-02-22";
    dateElement.dispatchEvent(new Event("input"));

    expect(component.gameSheet.date).toEqual(testDate);
  });

  it(`should bind playernames`, () => {
    const component = fixture.componentInstance;
    const element   = fixture.nativeElement as HTMLElement;
    const playerElement = element.querySelector("#players") as HTMLInputElement;

    let playerNames = "Me, You and all the others";
    component.gameSheet.players = playerNames;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(playerElement.textContent).toBe(playerNames);
    });

    playerNames = "You, Me and the rest";
    playerElement.value = playerNames;
    playerElement.dispatchEvent(new Event("input"));
    expect(component.gameSheet.players).toBe(playerNames);
  });
});

function randomInt() {
  return Math.floor(Math.random() * 100);
}

function checkPointField(fixture: ComponentFixture<GameSheetComponent>, 
                         cssInputSelector : string,
                         intermediateSumFunction: () => number,
                         propertyFunction: (points : number) => number) {

  const element = fixture.nativeElement as HTMLElement;
  const component = fixture.componentInstance as GameSheetComponent;
  const pointInput = element.querySelector(cssInputSelector) as HTMLInputElement;
  expect(pointInput).toBeDefined();

  let points = randomInt();

  /*
    * check binding view -> model
    */
  pointInput.valueAsNumber = points;
  pointInput.dispatchEvent(new Event('input'));

  expect(intermediateSumFunction()).toBe(points);
  expect(component.gameSheet.sum()).toBe(points);

  /*
    * check binding model -> view
    */
  points = randomInt();
  propertyFunction(randomInt());
  fixture.detectChanges()
  fixture.whenStable().then(() => {
    expect(pointInput.valueAsNumber).toBe(points);
  });

}
