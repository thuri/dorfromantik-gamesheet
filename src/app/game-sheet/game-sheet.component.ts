import { Component } from '@angular/core';
import { AchievementType, GameSheet, Territory } from '../game-sheet';
import { FormsModule } from '@angular/forms';
import { NgFor, NgIf, NgSwitch, NgSwitchCase } from '@angular/common';

@Component({
  selector: 'app-game-sheet',
  standalone: true,
  imports: [FormsModule, NgFor, NgIf, NgSwitch, NgSwitchCase],
  templateUrl: './game-sheet.component.html',
  styleUrl: './game-sheet.component.css'
})
export class GameSheetComponent {

  gameSheet : GameSheet
  date: string
  territories : Territory[]
  achievements: AchievementType[];
  achievementLabels = new Map<AchievementType, string>([
    [AchievementType.REDHEARTS, "Rote Herzen (1/passender Kante):"],
    [AchievementType.CIRCUS, "Zirkus (umschlossen = 10):"],
    [AchievementType.LINEMAN, "Bahnwärter (2/Bahnübergang):"],
    [AchievementType.SHEPHERD, "Schäferin (1/Schaf):"],
    [AchievementType.HILL, "Hügel (im Abstand 2 = 2/Auftrag):"],
    [AchievementType.CONSTRUCTION_SITE, "Baustelle (pro Gebiet 7+ = 7):"],
    [AchievementType.BALLOON, "Ballon-Startplatz (2/Entfernung):"],
    [AchievementType.GOLDENHEARTS, "Goldenes Herz (2/passender Kante):"],
    [AchievementType.CABIN_IN_THE_WOODS, "Waldhütte:"],
    [AchievementType.HARVEST, "Erntefest:"],
    [AchievementType.WATCHTOWER, "Wachturm:"],
    [AchievementType.LOCOMOTIVE, "Lokomotive:"],
    [AchievementType.SHIP, "Schiff:"],
    [AchievementType.TRAIN_STATION, "Bahnhof (beendet = 1/Plättchen):"],
    [AchievementType.PORT, "Hafen (beendet = 1/Plättchen):"],
  ]);

  constructor() {
    this.gameSheet = new GameSheet();
    this.date = this.gameSheet.date.toISOString().slice(0,10);
    this.territories = Object.values(Territory) as Territory[];
    this.achievements = Object.keys(AchievementType) as AchievementType[];
  }

  onDateChanged() {
    this.gameSheet.date = new Date(this.date);
  }
}
