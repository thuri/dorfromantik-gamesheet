export class GameSheet {
    round = 1
    players?: string;
    date: Date = new Date();
    quests : Record<Territory,number> = {
        FIELD       : 0,
        FOREST      : 0,
        VILLAGE     : 0,
        RAILROAD    : 0,
        RIVER       : 0,
    };
    connected : Record<Territory,number> = {
        FIELD       : 0,
        FOREST      : 0,
        VILLAGE     : 0,
        RAILROAD    : 0,
        RIVER       : 0
    };
    achievements : Record<AchievementType, number>;

    constructor() {
        const a = new Object();
        Object.values(AchievementType).forEach((achievement) => {
            Object.defineProperty(a, achievement, {value: 0, writable: true});
        });
        this.achievements = a as Record<AchievementType,number>
    }

    questSum() : number {
        return  this.quests.FOREST
            +   this.quests.FIELD
            +   this.quests.VILLAGE
            +   this.quests.RAILROAD
            +   this.quests.RIVER
        ;
    }

    connectedSum() : number {
        return  this.connected.FOREST
            +   this.connected.FIELD
            +   this.connected.VILLAGE
            +   this.connected.RAILROAD
            +   this.connected.RIVER
            ;
    }

    achievmentSum() : number {
        return Object.values(AchievementType)
        .map((achievement) => this.achievements[achievement as AchievementType])
        .reduce((prev, curr) => prev + curr);
    }

    sum() : number {
        return  this.questSum() 
            +   this.connectedSum()
            +   this.achievmentSum()
            ;
    }
}

export enum Territory {
    Forest   = "FOREST",
    Field    = "FIELD",
    Village  = "VILLAGE",
    Railroad = "RAILROAD",
    River    = "RIVER",
}

export enum AchievementType {
    REDHEARTS   = "REDHEARTS",
    CIRCUS      = "CIRCUS",
    LINEMAN     = "LINEMAN",
    SHEPHERD    = "SHEPHERD",
    HILL        = "HILL",
    CONSTRUCTION_SITE = "CONSTRUCTION_SITE",
    BALLOON     = "BALLOON",
    GOLDENHEARTS = "GOLDENHEARTS",
    CABIN_IN_THE_WOODS = "CABIN_IN_THE_WOODS",
    HARVEST     = "HARVEST",
    WATCHTOWER  = "WATCHTOWER",
    LOCOMOTIVE  = "LOCOMOTIVE",
    SHIP        = "SHIP",
    TRAIN_STATION = "TRAIN_STATION",
    PORT        = "PORT",
}
