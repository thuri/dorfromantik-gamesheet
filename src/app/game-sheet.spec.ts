import { GameSheet, Territory } from "./game-sheet";

describe('GameSheet', () => {

    let gameSheet : GameSheet;

    beforeEach(() => {
        gameSheet = new GameSheet();
    });
    
    it("round should be initialized with 1", () => {
        expect(gameSheet.round).toBe(1);
    });

    it("#constructor should initialize quest array", () => {
        const territories = Object.values(Territory);
        territories.forEach((value) => {
            expect(gameSheet.quests[value as Territory])
            .withContext(`quest field for territory ${value} should be initialized with 0`)
            .toBe(0);
        });
        expect(Object.keys(gameSheet.quests).length)
        .withContext(`quests must have one field for each territory type. Numbers of keys different from number of territory values`)
        .toBe(territories.length);
    });

    it("#constructor should initialize connected territories", () => {
        expect(gameSheet.connected.FOREST).toBe(0);
        expect(gameSheet.connected.FIELD).toBe(0);
        expect(gameSheet.connected.VILLAGE).toBe(0);
        expect(gameSheet.connected.RAILROAD).toBe(0);
        expect(gameSheet.connected.RIVER).toBe(0);

        expect(Object.keys(gameSheet.connected).length)
        .withContext("there must be exactly 5 fields for the flags in the sheet")
        .toBe(5);
    })

    Object.values(Territory)
    .forEach(territory => {
        it("incrementing quest should increment sums", () => {
            gameSheet.quests[territory] = 1;
            expect(gameSheet.questSum()).toBe(1);
            expect(gameSheet.sum()).toBe(1);
        })
    });

    Object.values(Territory)
    .forEach((territory) => {
        it("incrementing flags should increment sums", () => {
            gameSheet.connected[territory] = 1;
            expect(gameSheet.connectedSum()).toBe(1);
            expect(gameSheet.sum()).toBe(1);
        })
    });
});